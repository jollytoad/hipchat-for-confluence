# HipChat Confluence Plugin

This is a simple Confluence plugin that:

1. Allows you to link a space to a HipChat room. When a new blog posts in that space is published, the plugin will notify users in the HipChat room.
2. Show a user's HipChat presence status on the Confluence hover card.

I'll likely be adding more integrations over time. If you'd like to contribute, feel free to fork and send me a pull request.

![HipChat status in Confluence](https://img.skitch.com/20120216-mqnufnfcd7qh6hxkmh6tuyrdgr.png)
![Blog notifications inside a HipChat room](https://img.skitch.com/20120216-bcejc77nua6pfecfj1wk29xdmm.png)
