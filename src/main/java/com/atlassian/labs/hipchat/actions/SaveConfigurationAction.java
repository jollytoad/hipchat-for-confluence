package com.atlassian.labs.hipchat.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.atlassian.xwork.RequireSecurityToken;
import com.opensymphony.xwork.Action;
import org.apache.commons.lang.StringUtils;

public class SaveConfigurationAction extends ConfluenceActionSupport
{
    private ConfigurationManager configurationManager;
    private PermissionManager permissionManager;

    private String spaceKey;
    private String hipChatAuthToken;
    private String hipChatBaseUrl;

    @Override
    public boolean isPermitted()
    {
        return permissionManager.isConfluenceAdministrator(getRemoteUser());
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setHipChatAuthToken(String value)
    {
        this.hipChatAuthToken = value;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setHipChatBaseUrl(String hipChatBaseUrl)
    {
        this.hipChatBaseUrl = hipChatBaseUrl;
    }

    @Override
    public void validate()
    {
        if (StringUtils.isBlank(hipChatAuthToken))
        {
            addActionError(getText("hipchat.token.form.invalidtokenerror"));
        }
    }

    @Override
    @RequireSecurityToken(true)
    public String execute() throws Exception
    {
        if (hipChatBaseUrl != null)
        {
            configurationManager.setHipChatBaseUrl(hipChatBaseUrl);
        }
        configurationManager.setHipChatAuthToken(hipChatAuthToken);

        if (StringUtils.isNotBlank(spaceKey))
        {
            return "redirect";
        }
        return Action.SUCCESS;
    }

    public String getSpaceKey()
    {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey)
    {
        this.spaceKey = spaceKey;
    }

    // =================================================================================================================
    // We have to use setter injection if we don't use the defaultStack
    // See https://jira.atlassian.com/browse/CONF-23137
    public void setConfigurationManager(ConfigurationManager configurationManager)
    {
        this.configurationManager = configurationManager;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }
}