package com.atlassian.labs.hipchat.actions;

import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.atlassian.labs.hipchat.components.HipChatProxyClient;
import com.opensymphony.xwork.Action;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;

public class ViewSpaceConfigurationAction extends AbstractSpaceAdminAction
{
    private static final Logger log = LoggerFactory.getLogger(ViewSpaceConfigurationAction.class);
    private final HipChatProxyClient hipChatProxyClient;
    private final ConfigurationManager configurationManager;

    private String roomId;
    private String roomJson;
    private boolean successFullUpdate;

    public ViewSpaceConfigurationAction(HipChatProxyClient hipChatProxyClient, ConfigurationManager configurationManager)
    {
        this.hipChatProxyClient = hipChatProxyClient;
        this.configurationManager = configurationManager;
    }

    public void setResult(String result) {
        if ("success".equals(result)) {
            successFullUpdate = true;
        }
    }
    
    @Override
    public String execute()
    {
        setRoomId(configurationManager.getHipChatRooms(key));
        if(StringUtils.isBlank(configurationManager.getHipChatAuthToken())) {
            return Action.INPUT;
        } else {
            try {
                setRoomJson(hipChatProxyClient.getRooms().right().toString());
            } catch (WebApplicationException e) {
                
                if(e.getResponse().getStatus() >= 500) {
                    log.error("Could not get hipchat room with key {} with message {}", key, e);
                    log.warn("More details: ", e);
                }
                return Action.ERROR;
            }

            return Action.SUCCESS;
        }
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomId() {
        return roomId;
    }

    public String getRoomJson() {
        return roomJson;
    }

    public void setRoomJson(String roomJson) {
        this.roomJson = roomJson;
    }

    public boolean isSuccessFullUpdate() {
        return successFullUpdate;
    }
}