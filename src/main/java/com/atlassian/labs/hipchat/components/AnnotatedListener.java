package com.atlassian.labs.hipchat.components;

import com.atlassian.confluence.event.events.content.blogpost.BlogPostCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.TinyUrl;
import com.atlassian.confluence.user.PersonalInformationManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.List;

public class AnnotatedListener implements DisposableBean, InitializingBean {
    private static final Logger log = LoggerFactory.getLogger(AnnotatedListener.class);

    private final WebResourceUrlProvider webResourceUrlProvider;
    private final EventPublisher eventPublisher;
    private final HipChatProxyClient hipChatProxyClient;
    private final ConfigurationManager configurationManager;
    private final PersonalInformationManager personalInformationManager;
    private final UserAccessor userAccessor;

    public AnnotatedListener(EventPublisher eventPublisher,
                             ConfigurationManager configurationManager,
                             PersonalInformationManager personalInformationManager, UserAccessor userAccessor,
                             HipChatProxyClient hipChatProxyClient, WebResourceUrlProvider webResourceUrlProvider) {
        this.eventPublisher = eventPublisher;
        this.configurationManager = configurationManager;
        this.hipChatProxyClient = hipChatProxyClient;
        this.userAccessor = userAccessor;
        this.personalInformationManager = personalInformationManager;
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    @EventListener
    public void blogPostCreateEvent(BlogPostCreateEvent event) {
        final BlogPost blogPost = event.getBlogPost();
        final User user = userAccessor.getUser(blogPost.getCreatorName());

        String postLink = createLinkToBlogPost(blogPost);
        String chatMessage = String.format("%s&nbsp;%s - new blog post by %s", getBlogIconUrl(), postLink, getPersonalSpaceUrl(user));

        List<String> roomsToNotify = configurationManager.getHipChatRoomList(blogPost.getSpaceKey());

        for(String room : roomsToNotify) {
            if (room.indexOf(":") == -1) {
                hipChatProxyClient.notifyRoom(room, chatMessage);
            }
        }
    }

    @EventListener
    public void pageCreateEvent(PageCreateEvent event){
        final Page page = event.getPage();
        final User user = userAccessor.getUser(page.getCreatorName());

        String postLink = createLinkToPage(page);
        String chatMessage = String.format("%s&nbsp;%s - new page created by %s", getPageIconUrl(), postLink, getPersonalSpaceUrl(user));

        List<String> roomsToNotify = configurationManager.getHipChatRoomList(page.getSpaceKey());

        for(String room : roomsToNotify) {
            if (room.indexOf(":pc") > 0){
                hipChatProxyClient.notifyRoom(room.split(":pc")[0], chatMessage);
            }
        }
    }

    @EventListener
    public void pageUpdateEvent(PageUpdateEvent event){
        final Page page = event.getPage();
        final User user = userAccessor.getUser(page.getLastModifierName());

        String postLink = createLinkToPage(page);
        String chatMessage = String.format("%s&nbsp;%s - page updated by %s", getPageIconUrl(), postLink, getPersonalSpaceUrl(user));

        List<String> roomsToNotify = configurationManager.getHipChatRoomList(page.getSpaceKey());

        for(String room : roomsToNotify) {
            if (room.indexOf(":pu") > 0){
                hipChatProxyClient.notifyRoom(room.split(":pu")[0], chatMessage);
            }
        }
    }
    private String getPersonalSpaceUrl(User user) {
        if (null == user) {
            return "";
        }
        return String.format("<a href=\"%s/%s\">%s</a>", webResourceUrlProvider.getBaseUrl(UrlMode.ABSOLUTE), personalInformationManager.getPersonalInformation(user).getUrlPath(), GeneralUtil.escapeXml(user.getFullName()));
    }

    private String createLinkToBlogPost(BlogPost blogPost) {
        String title = blogPost.getTitle();
        String url = webResourceUrlProvider.getBaseUrl(UrlMode.ABSOLUTE) + "/x/" + new TinyUrl(blogPost).getIdentifier();
        return String.format("<a href=\"%s\"><b>%s</b></a>", GeneralUtil.escapeForHtmlAttribute(url), GeneralUtil.escapeXMLCharacters(title));
    }

    private String createLinkToPage(Page page) {
        String title = page.getTitle();
        String url = webResourceUrlProvider.getBaseUrl(UrlMode.ABSOLUTE) + "/x/" + new TinyUrl(page).getIdentifier();
        return String.format("<a href=\"%s\"><b>%s</b></a>", GeneralUtil.escapeForHtmlAttribute(url), GeneralUtil.escapeXMLCharacters(title));
    }

    private String getBlogIconUrl() {
        return String.format("<img src=\"%s\" width=16 height=16 />", webResourceUrlProvider.getStaticPluginResourceUrl("com.atlassian.labs.hipchat.confluence-hipchat:image-resources","blog.png",UrlMode.ABSOLUTE));
    }

    private String getPageIconUrl() {
        return String.format("<img src=\"%s\" width=16 height=16 />", webResourceUrlProvider.getStaticPluginResourceUrl("com.atlassian.labs.hipchat.confluence-hipchat:image-resources","page.png",UrlMode.ABSOLUTE));
    }

    // Unregister the listener if the plugin is uninstalled or disabled.
    @Override public void destroy() throws Exception {
        log.debug("Unregister blog created event listener");
        eventPublisher.unregister(this);
    }

    @Override public void afterPropertiesSet() throws Exception {
        log.debug("Register blog created event listener");
        eventPublisher.register(this);
    }
}