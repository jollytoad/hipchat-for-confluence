package com.atlassian.labs.hipchat.rest;


import com.atlassian.fugue.Pair;
import com.atlassian.labs.hipchat.components.HipChatProxyClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/user/show")
public class HipChatUserProxy
{
    private HipChatProxyClient client;

    public HipChatUserProxy(HipChatProxyClient client)
    {
        this.client = client;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUser(@QueryParam("user_id") String userId)
    {
        final Pair<Integer, HipChatProxyClient.JSONString> userResp = client.getUser(userId);
        return Response.status(userResp.left()).entity(userResp.right().toString()).build();
    }
}