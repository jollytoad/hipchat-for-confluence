package com.atlassian.labs.hipchat.components;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.event.events.content.blogpost.BlogPostCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.user.PersonalInformation;
import com.atlassian.confluence.user.PersonalInformationManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.user.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;


@RunWith(org.mockito.runners.MockitoJUnitRunner.class)
public class AnnotatedListenerTest {

    private static final String BASE_URL = "http://localhost/confluence";
    private static final String userName = "admin";
    private static final String spaceKey = "TST";

    private static final String blogPostTitle = "HipChat has joined the Atlassian family!";
    private static final String fullName = "A. D. Ministrator";

    private static final String pageTitle = "A new page";


    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private HipChatProxyClient hipChatProxyClient;
    @Mock
    private ConfigurationManager configurationManager;
    @Mock
    private PersonalInformationManager personalInformationManager;
    @Mock
    private UserAccessor userAccessor;
    @Mock
    private BlogPost blogPost;
    @Mock
    private Page page;
    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;

    // class under test
    private AnnotatedListener annotatedListener;

    @Before
    public void setUp() throws Exception {
        annotatedListener = new AnnotatedListener(eventPublisher, configurationManager, personalInformationManager, userAccessor, hipChatProxyClient, webResourceUrlProvider);
        when(webResourceUrlProvider.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn(BASE_URL);

        User testUser = mock(User.class);
        PersonalInformation personalInformation = mock(PersonalInformation.class);

        when(blogPost.getId()).thenReturn(12L);
        when(blogPost.getSpaceKey()).thenReturn(spaceKey);
        when(blogPost.getTitle()).thenReturn(blogPostTitle);
        when(blogPost.getCreatorName()).thenReturn(userName);

        when(page.getId()).thenReturn(12L);
        when(page.getSpaceKey()).thenReturn(spaceKey);
        when(page.getTitle()).thenReturn(pageTitle);
        when(page.getCreatorName()).thenReturn(userName);
        when(page.getLastModifierName()).thenReturn(userName);

        when(userAccessor.getUser(userName)).thenReturn(testUser);
        when(testUser.getFullName()).thenReturn(fullName);
        when(personalInformationManager.getPersonalInformation(testUser)).thenReturn(personalInformation);
        when(personalInformation.getUrlPath()).thenReturn("~admin");
    }

    @After
    public void tearDown() throws Exception {
        annotatedListener = null;
    }

    @Test
    public void testBlogPostCreateEvent() throws Exception {
        BlogPostCreateEvent event = mock(BlogPostCreateEvent.class);
        when(event.getBlogPost()).thenReturn(blogPost);

        when(configurationManager.getHipChatRoomList(spaceKey)).thenReturn(Arrays.asList("ConfDev", "BB"));
        annotatedListener.blogPostCreateEvent(event);
        verify(hipChatProxyClient).notifyRoom("ConfDev", "<img src=\"null\" width=16 height=16 />&nbsp;<a href=\"http://localhost/confluence/x/D\"><b>HipChat has joined the Atlassian family!</b></a> - new blog post by <a href=\"http://localhost/confluence/~admin\">A. D. Ministrator</a>");
        verify(hipChatProxyClient).notifyRoom(eq("BB"), anyString());
    }

    @Test
    public void testBlogPostCreateEventWithInvalidMarkup() throws Exception {
        BlogPostCreateEvent event = mock(BlogPostCreateEvent.class);
        when(event.getBlogPost()).thenReturn(blogPost);

        when(blogPost.getTitle()).thenReturn("<IMG \"\"\"><SCRIPT>alert(\"XSS\")</SCRIPT>\">");
        when(configurationManager.getHipChatRoomList(spaceKey)).thenReturn(Arrays.asList("ConfDev"));
        annotatedListener.blogPostCreateEvent(event);
        verify(hipChatProxyClient).notifyRoom("ConfDev", "<img src=\"null\" width=16 height=16 />&nbsp;<a href=\"http://localhost/confluence/x/D\"><b>&lt;IMG \"\"\"&gt;&lt;SCRIPT&gt;alert(\"XSS\")&lt;/SCRIPT&gt;\"&gt;</b></a> - new blog post by <a href=\"http://localhost/confluence/~admin\">A. D. Ministrator</a>");
    }

    @Test
    public void testBlogPostCreateEventNoRooms() throws Exception {
        BlogPostCreateEvent event = mock(BlogPostCreateEvent.class);
        when(event.getBlogPost()).thenReturn(blogPost);

        when(configurationManager.getHipChatRoomList(spaceKey)).thenReturn(Collections.<String>emptyList());

        annotatedListener.blogPostCreateEvent(event);
        verifyZeroInteractions(hipChatProxyClient);
    }

    @Test
    public void testPageCreateEvent() throws Exception {
        PageCreateEvent event = mock(PageCreateEvent.class);
        when(event.getPage()).thenReturn(page);

        when(configurationManager.getHipChatRoomList(spaceKey)).thenReturn(Arrays.asList("ConfDev:pc", "BB:pc"));
        annotatedListener.pageCreateEvent(event);
        verify(hipChatProxyClient).notifyRoom("ConfDev", "<img src=\"null\" width=16 height=16 />&nbsp;<a href=\"http://localhost/confluence/x/D\"><b>A new page</b></a> - new page created by <a href=\"http://localhost/confluence/~admin\">A. D. Ministrator</a>");
        verify(hipChatProxyClient).notifyRoom(eq("BB"), anyString());
    }

    @Test
    public void testPageUpdateEvent() throws Exception {
        PageUpdateEvent event = mock(PageUpdateEvent.class);
        when(event.getPage()).thenReturn(page);

        when(configurationManager.getHipChatRoomList(spaceKey)).thenReturn(Arrays.asList("ConfDev:pu", "BB:pu"));
        annotatedListener.pageUpdateEvent(event);
        verify(hipChatProxyClient).notifyRoom("ConfDev", "<img src=\"null\" width=16 height=16 />&nbsp;<a href=\"http://localhost/confluence/x/D\"><b>A new page</b></a> - page updated by <a href=\"http://localhost/confluence/~admin\">A. D. Ministrator</a>");
        verify(hipChatProxyClient).notifyRoom(eq("BB"), anyString());
    }
}
