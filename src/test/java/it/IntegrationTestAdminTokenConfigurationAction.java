package it;

/**
 * Minimal integration test that ensures that the plugin is working and actions are wired correctly.
 */
public class IntegrationTestAdminTokenConfigurationAction extends AbstractIntegrationTestCase {

    public void testConfigurationViewPage() {
        gotoPage("/admin/hipchat.action");
        assertTextPresent("HipChat API Auth Token Configuration");
    }

    public void testSaveConfigurationRequiresXsrfToken() {
        assertPageRequiresXsrfToken("/admin/doconfigure-hipchat.action");
    }
}
