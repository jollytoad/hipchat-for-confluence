package it;

public class IntegrationTestSpaceAdminAction extends AbstractIntegrationTestCase {
    public void testSpaceConfigurationViewPage() {
        gotoPage("/spaces/hipchat.action?key=ds");
        assertTextPresent("HipChat Configuration");
    }

    public void testSaveSpaceConfigurationRequiresXsrfToken() {
        assertPageRequiresXsrfToken("/spaces/doconfigure-hipchat.action");
    }

}

